Given the Ballerina code you provided, here are the HTTP requests to interact with the endpoints:

1. **Add a new lecturer:**
   - **Method:** POST
   - **URL:** `http://localhost:9090/dsa/lecturers`
   - **Body:** JSON representation of a lecturer.
   ```json
   {
     "staffNumber": "123",
     "officeNumber": "B205",
     "staffName": "Dr. John Doe",
     "title": "Professor",
     "courses": [
       {
         "courseName": "Ballerina 101",
         "courseCode": "B101",
         "NQFLevel": 4
       }
     ]
   }
   ```

2. **Get a lecturer by staff number:**
   - **Method:** GET
   - **URL:** `http://localhost:9090/dsa/lecturers/[staffNumber]` (Replace `[staffNumber]` with the actual staff number.)

3. **Update a lecturer by staff number:**
   - **Method:** PUT
   - **URL:** `http://localhost:9090/dsa/lecturers/[staffNumber]` (Replace `[staffNumber]` with the actual staff number.)
   - **Body:** JSON representation of the updated lecturer details.

4. **Delete a lecturer by staff number:**
   - **Method:** DELETE
   - **URL:** `http://localhost:9090/dsa/lecturers/[staffNumber]` (Replace `[staffNumber]` with the actual staff number.)

5. **Get all lecturers teaching a specific course:**
   - **Method:** GET
   - **URL:** `http://localhost:9090/dsa/lecturers/course/[courseCode]` (Replace `[courseCode]` with the actual course code.)

6. **Get all lecturers from a specific office:**
   - **Method:** GET
   - **URL:** `http://localhost:9090/dsa/lecturers/office/[officeNumber]` (Replace `[officeNumber]` with the actual office number.)

You can use tools like `curl`, Postman, or any HTTP client to make these requests.

For example, using `curl` to add a lecturer:
```bash
curl -X POST -H "Content-Type: application/json" -d '{"staffNumber":"123", "officeNumber":"B205", "staffName":"Dr. John Doe", "title":"Professor", "courses":[{"courseName":"Ballerina 101", "courseCode":"B101", "NQFLevel":4}]}' http://localhost:9090/dsa/lecturers
```

And so on for the other endpoints.